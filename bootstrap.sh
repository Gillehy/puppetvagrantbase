echo "Bootstrap: Opening firewall ports:"


sudo firewall-cmd --zone=public --permanent --add-port=80/tcp 
sudo firewall-cmd --reload
sudo systemctl restart network

echo "Bootstrap: Installing r10k"

sudo /opt/puppetlabs/puppet/bin/gem install r10k

